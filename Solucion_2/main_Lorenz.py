import numpy as np
from Lorenz import Lorenz_RKG

if __name__ == '__main__':

    # Parameters to animate
    frames = 10000
    ffmpeg_path = r'C:\PATH_ffmpeg\ffmpeg.exe'

    # Runge-Kutta 4
    A = [[0,0,0,0],[1/2,0,0,0],[0,1/2,0,0],[0,0,1,0]]
    b = [1/6,1/3,1/3,1/6]
    c = [0,1/2,1/2,1]

    # Initial conditions and parameters
    t0,tf,x0,y0,z0,sigma,rho,beta,n = 0,100,0.0,0.001,0.0,16,45,4,10000

    # Instance of Lorenz_RKG class
    F = Lorenz_RKG(t0,tf,x0,y0,z0,sigma,rho,beta,n,A,b,c)
    F.plot_2D('x','y')
    F.plot_2D('x','z')
    F.plot_2D('y','z')
    F.plot_2D_vs_time()
    F.plot_3D()
    F.plot_3D_animate(frames, ffmpeg_path=ffmpeg_path)