import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.animation import FuncAnimation
from RKG import RKG, RKG_A

class Lorenz_RKG(RKG_A):
    '''
    Class to solve Lorenz equations using Runge-Kutta method
    t0: Initial time
    tf: Final time
    x0: Initial value of x
    y0: Initial value of y
    z0: Initial value of z
    sigma: Parameter
    rho: Parameter
    beta: Parameter
    n: Number of steps
    A: Coefficients of the matrix A
    b: Coefficients of the array b
    c: Coefficients of the array c
    '''
    def __init__(self,t0,tf,x0,y0,z0,sigma,rho,beta,n,A,b,c):
        
        # Parameters
        self.t0 = t0
        self.tf = tf
        self.x0 = x0
        self.y0 = y0
        self.z0 = z0
        self.sigma = sigma
        self.rho = rho
        self.beta = beta
        self.n = n
        h = (self.tf-self.t0)/self.n
        super().__init__(A,b,c,self.Eq_Lorenz,self.t0,[self.x0,self.y0,self.z0],h,self.tf) # Herency from RKG class
    
    def Eq_Lorenz(self, t, Y):
        # Lorenz equations
        x, y, z = Y
        # Derivatives
        dxdt = self.sigma * (y - x)
        dydt = x * (self.rho - z) - y
        dzdt = x * y - self.beta * z
        return np.array([dxdt, dydt, dzdt])
    
    def apply_Lorenz(self):
        # Solve Lorenz equations
        return self.apply_RKG_A()
    
    def plot_3D(self):
        # Method to plot 3D graph
        t,sol = self.apply_Lorenz()
        x = np.array(sol).T[0]
        y = np.array(sol).T[1]
        z = np.array(sol).T[2]
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(x,y,z,'r-', label=r'$\sigma$ = 16, $\rho$ = 45, $\beta$ = 4')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        ax.legend()
        plt.savefig('Plots_Lorenz/Second_Case/Lorenz_3D.png')
        plt.show()
    
    def plot_2D(self, x_axis, y_axis):
        # Method to plot 2D graphs: x vs y, x vs z, y vs z
        coordinates = ['x','y','z']
        numbers = [0,1,2]
        map = dict(zip(coordinates,numbers))

        t,sol = self.apply_Lorenz()
        x = np.array(sol).T[map[x_axis]]
        y = np.array(sol).T[map[y_axis]]
        
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(x,y,'r-', label=r'$\sigma$ = 16, $\rho$ = 45, $\beta$ = 4')
        ax.set_xlabel(x_axis)
        ax.set_ylabel(y_axis)
        ax.grid()
        ax.legend()
        plt.savefig('Plots_Lorenz/Second_Case/{}_vs_{}.png'.format(x_axis,y_axis))
        plt.show()
    
    def plot_2D_vs_time(self):
        # Method to plot the coordinates vs time

        t,sol = self.apply_Lorenz()
        x = np.array(sol).T[0]
        y = np.array(sol).T[1]
        z = np.array(sol).T[2]

        fig = plt.figure()
        ax = fig.add_subplot(111)

        ax.plot(t,x,label='x')
        ax.plot(t,y,label='y')
        ax.plot(t,z,label='z')
        ax.set_title(r'Coordinates vs time, $\sigma$ = 16, $\rho$ = 45, $\beta$ = 4')
        ax.set_xlabel('t')
        ax.set_ylabel('x,y,z')
        ax.grid()
        ax.legend()

        plt.savefig('Plots_Lorenz/Second_Case/coordinates_vs_time.png')
        plt.show()

    def plot_3D_animate(self, frames, ffmpeg_path=None):
        """
        Method to animate the 3D plot of the Lorenz equations
        
        Args:
        - self
        - frames: frames of the animation
        - ffmpeg_path: path to ffmpeg
        """
        # Path to ffmpeg
        matplotlib.rcParams['animation.ffmpeg_path'] = ffmpeg_path  # Modificar en su respectivo computador

        # Create the figure
        fig, ax = plt.subplots(subplot_kw={'projection': '3d'}, figsize=(10, 8))

        # Initialize the lines
        line1, = ax.plot([], [], [], 'r-', label=r'$\sigma$ = 16, $\rho$ = 45, $\beta$ = 4')

        # Solution of the Lorenz equations
        t,sol = self.apply_Lorenz()
        x = np.array(sol).T[0]
        y = np.array(sol).T[1]
        z = np.array(sol).T[2]

        # Set the limits
        ax.set_xlim((-40, 40))
        ax.set_ylim((-45, 50))
        ax.set_zlim((0, 80))

        # Set the labels
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')

        ax.legend()
        ax.set_title('Trajectory of the Lorenz equations')

        # Initialize the function of the animation
        def init():
            line1.set_data([], [])
            line1.set_3d_properties([])
            return [line1,]

        # Function to animate
        def animate(i):
            line1.set_data(x[:i], y[:i])
            line1.set_3d_properties(z[:i])
            return [line1,]

        # Create the animation
        ani = FuncAnimation(fig, animate, frames=frames, init_func=init, blit=True, interval=20)

        # Plot and save the animation
        ani.save('Plots_Lorenz/Second_Case/Lorenz_3D_animation.mp4', writer='ffmpeg', fps=30)
        plt.show()