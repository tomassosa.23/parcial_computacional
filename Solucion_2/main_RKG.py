import numpy as np
from RKG import RKG

if __name__ == '__main__':

    # Runge-Kutta 4
    A = [[0,0,0,0],[1/2,0,0,0],[0,1/2,0,0],[0,0,1,0]]
    b = [1/6,1/3,1/3,1/6]
    c = [0,1/2,1/2,1]

    f = lambda x,y: x-y
    f_analitical = lambda x: x-1+np.exp(-x)
    x0, y0, xf = 0.0, 0.0, 1.0
    h = 0.00001

    rkg = RKG(A, b, c, f, x0, y0, h, xf)
    rkg.plot_x_vs_y()
    rkg.plot_comparative(f_analitical)