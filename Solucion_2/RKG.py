import numpy as np
import matplotlib.pyplot as plt

class RKG:
    '''
    Class to solve ODEs using Runge-Kutta method
    A: Coefficients of the matrix A
    b: Coefficients of the array b
    c: Coefficients of the array c
    f: Function
    x0: Initial value of x
    y0: Initial value of y
    h: Step size
    xf: Final value of x
    '''
    def __init__(self, A, b, c, f, x0, y0, h, xf):

        # Parameters for Runge-Kutta method
        self.A = np.array(A)
        self.b = np.array(b)
        self.c = np.array(c)
        self.f = f
        self.x0 = x0
        self.y0 = y0
        self.h = h
        self.xf = xf


    def k_RKG(self,x0,y0):
        # Method to calculate k values for Runge-Kutta method
        k = np.zeros(len(self.A))

        for i in np.arange(0,len(self.A),1):
            k[i]=self.f(x0+self.c[i]*self.h,y0+self.h*np.dot(self.A[i],k))
        return k
    
    def Runge_kutta_generalized(self,x0,y0):

        # Assertions
        # 1. The number of rows of A must be equal to the number of columns of A, to the number of rows of b and to the number of rows of c
        assert self.A.shape[0] == self.A.shape[1] == self.b.shape[0] == self.c.shape[0], "Invalid coefficients"
        # 2. The sum of the elements of b must be equal to 1
        assert round(sum(self.b),3) == 1, "Invalid coefficients"
        # 3. The sum of the elements of each row of A must be equal to the corresponding element of c
        assert [sum(self.A[i]) for i in np.arange(0,len(self.A),1)] == list(self.c), "Invalid coefficients"

        # Runge-Kutta method
        k = self.k_RKG(x0,y0) # k values
        # Next values
        x = x0 + self.h
        y = y0 + self.h*np.dot(self.b,k)
        return x,y

    def apply_RKG(self):

        # Assertions
        assert self.h > 0, "Invalid step size"
        assert type(self.h) == int or type(self.h) == float, "Invalid step size"
        assert self.x0 < self.xf, "Invalid interval"

        # Initial values
        x = self.x0
        y = self.y0
        F = [[x],[y]]

        # Solution
        while x < self.xf-self.h:
            x,y = self.Runge_kutta_generalized(x,y)
            F[0].append(x)
            F[1].append(y)
        return F
    
    def plot_x_vs_y(self):
        x,y = self.apply_RKG()
        plt.plot(x,y)
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.grid()
        plt.savefig('Plots_RKG/x_vs_y.png')
        plt.show()

    def plot_comparative(self, f_analitical):
        x,y = self.apply_RKG()
        plt.plot(x,y,label='RKG')
        plt.plot(x,f_analitical(np.array(x)),label='Analytical', linestyle='--')
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.legend()
        plt.grid()
        plt.savefig('Plots_RKG/comparative.png')
        plt.show()

class RKG_A(RKG):
    def __init__(self, A, b, c, f, x0, y0, h, xf):
        super().__init__(A, b, c, f, x0, y0, h, xf)
    
    def k_RKG_A(self,x0,y0):
        # Method to calculate k values for Runge-Kutta method with accouple ODEs

        k = np.zeros((len(self.A), len(self.y0))) if np.ndim(self.y0) > 0 else np.zeros((len(self.A),))
        for i in np.arange(0,len(self.A),1):
            xi = x0+self.c[i]*self.h
            yi = y0+self.h*np.dot(self.A[i],k).T
            k[i]=self.f(xi,yi)

        return k
    
    def Runge_kutta_generalized_A(self,x0,y0):

        # Assertions
        # 1. The number of rows of A must be equal to the number of columns of A, to the number of rows of b and to the number of rows of c
        assert self.A.shape[0] == self.A.shape[1] == self.b.shape[0] == self.c.shape[0], "Invalid coefficients"
        # 2. The sum of the elements of b must be equal to 1
        assert round(sum(self.b),3) == 1, "Invalid coefficients"
        # 3. The sum of the elements of each row of A must be equal to the corresponding element of c
        assert [sum(self.A[i]) for i in np.arange(0,len(self.A),1)] == list(self.c), "Invalid coefficients"

        # Runge-Kutta method
        k = self.k_RKG_A(x0,y0) # k values
        # Next values
        x = x0 + self.h
        y = y0 + self.h*np.dot(self.b,k)
        return x,y
    
    def apply_RKG_A(self):

        # Assertions
        assert self.h > 0, "Invalid step size"
        assert type(self.h) == int or type(self.h) == float, "Invalid step size"
        assert self.x0 < self.xf, "Invalid interval"

        # Initial values
        x = self.x0
        y = self.y0
        F = [[x],[y]]

        # Solution
        while x < self.xf-self.h:
            x,y = self.Runge_kutta_generalized_A(x,y)
            F[0].append(x)
            F[1].append(y)
        return F