from numerical_methods  import ODE_SOLUTIONS, SOLUTION_CDS
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':
    a, b, x0, y0, z0, n, sigma, rho, beta = 0, 100, 0.0, 0.0, 0.0, 1000, 10, 28, 8/3
    t = np.linspace(a, b, n)
    F = SOLUTION_CDS(a, b, x0, y0, z0, n, sigma, rho, beta)
    sol = F.Lorenz()
    print(sol)
    F.plot_RKG_3D()